#Apply all math operations e.g +,-, **, % e.t.c to the variable itself
#e.g
#x=5
#x+=3
#print(x)
x = 10
x += 5
print(x)
x = 10
x -= 5
print(x)
x = 10
x *= 5
print(x)
x = 10
x /= 5
print(x)
x = 10
x **= 5
print(x)
x = 10
x //= 5
print(x)
x = 10
x %= 5
print(x)
